# Начало работы с API Automation: Часть 1 - Обзор #

*Перевод [статьи](https://medium.com/testcult/getting-started-with-api-automation-3cf394240afb) Shashi Kumar Raja*

Итак, допустм, вы хотите заняться автоматизацией API, но каждый раз, когда вы набираете в строке поиска в Google *best api automation tool*, вы видите так много ссылок на 10-ки топовых инструментов, что запутываетесь и решаете, что определенно сделаете это **завтра 💤**

![](https://habrastorage.org/webt/zr/kq/we/zrkqwein7iz4odk_rqiw_osz730.png)

Давайте начнем с понимания того, что нам нужно, если мы хотим настроить (надеюсь завтра 😊 ) **платформу автоматизации API**. Задержи эту мысль на секунду ... Я сказал, прекрати думать ... *Я знаю, что твой босс не выделяет ни копейки ни на какие платные инструменты*. Смотри, я читаю твои мысли, не думай вслух 😼


![](https://cdn-images-1.medium.com/max/800/1*71d9s5nOcPkNtaNXmkrVHQ.jpeg)

Платные инструменты должны быть повешены, а не выполнены :)

> ### 1. где, черт возьми, я должен начать писать тесты? ###

Вам понадобится что-то, что предоставит вам наборы правил и рекомендаций для написания тестов, а также позволит вам сделать это, предоставив вам доступ к нескольким инструментам и методикам. Я слышу колокольный звон 🔔, нет!! Хорошо, я знаю, что заставит звонить колокол.





*Вы когда-нибудь слышали о [TESTNG](HTTPS://TESTNG.ORG/DOC/INDEX.HTML), [JUNIT](HTTPS://JUNIT.ORG/JUNIT5/), [MOCHA](HTTP://MOCHA.ORG/), [PYTEST](HTTPS://DOCS.PYTEST.ORG/EN/LATEST/), [ROBOT](HTTP://ROBOTFRAMEWORK.ORG/)!? Да, все они являются средами автоматизации тестирования.*

Вам нужно найти подходящую среду тестирования, основанную на ваших требованиях: какой существующий технологический стек использует ваша компания? Какую автоматизацию вы хотите сделать? Какой язык вам удобнее и т.д. Вы найдете среду автоматизации в большинстве популярных языков, которые позволят вам написать *модульное, функциональное и другие* виды тестирования API.

Чтобы узнать больше о тестовой среде, обратитесь к 2-й части серии, где я подробно представил [Mocha](https://medium.com/@shashiraja/getting-started-with-api-automation-part-2a-intro-to-test-framework-mocha-c793a6b77627) и [Pytest](https://medium.com/@shashiraja/intro-to-test-framework-pytest-5b1ce4d011ae).

> ### 2. How am I going to make the API calls in test framework? ###

Большинство этих платформ поддерживают вызовы API, включая библиотеку запросов **HTTP request library**, поскольку REST API используют протокол HTTP для связи.

Некоторые фреймворки, такие как [mocha](http://mocha.org/), дают вам свободу использовать библиотеку HTTP-запросов по вашему выбору, например [superagent](https://visionmedia.github.io/superagent/).

```JS
request
  .post('/api/pet')
  .send({ name: 'Manny', species: 'cat' }) // sends a JSON post body
  .set('X-API-Key', 'foobar')
  .set('accept', 'json')
  .end((err, res) => {
    // Calling the end function will send the request
  });
```

Они дают вам легкую поддержку для GET, PUT,POST, DELETE и всех других методов.Вы можете передать заголовки, кэш, параметры запроса, вы его называли -- вы его получили 👆

> ### 3. Круто, но некоторые из моих API дают JSON, а другие в ответ XML, как мне быть с этим? ###

Большинство из этих библиотек HTTP request позволят вам отправлять и получать данные в формате *JSON, XML, CSV, Text, Image, form-data, encoded-data* с несколькими поддерживаемыми стандартами авторизации.

Они также позволяют обрабатывать код статуса ответа HTTP и проверять, получили ли мы требуемый код статуса ответа или нет.



- Informational Response Codes (1xx)
100 — Continue
101 — Switching Protocols
102 — Processing


- Success Response Codes (2xx)
200 — OK 206 — Partial Content
201 — Created 207 — Multi-status
202 — Accepted 208 — Already Reported
203 — Non-authoritative Info 226 — IM Used
204 — No Content 250 — Low Storage Space
205 — Reset Content


- Redirection Response Codes (3xx)
300 — Multiple Choices 304 — Not Modified
301 — Moved Permanently 305 — Use Proxy
302 — Found 307 — Temporary Redirect
303 — See Other 308 — Permanent Redirect


- Client Error Response Codes (4xx)
400 — Multiple Choices 410 — Not Modified
401 — Moved Permanently 411 — Use Proxy
402 — Found 412 — Temporary Redirect
403 — See Other 413 — Permanent Redirect
404 — Multiple Choices 414 — Not Modified
405 — Moved Permanently 415 — Use Proxy
406 — Found 416 — Temporary Redirect
407 — See Other 417 — Permanent Redirect
408 — Found 418 — Temporary Redirect
409 — See Other


- Server Error Response Codes (5xx)
500 — Internal Server Error 508 — Loop Detected
501 — Not Implemented 509 — Bandwidth Limited
502 — Bad Gateway 510 — Not Extended
503 — Service Unavailable 511 — Network Auth Requried
504 — Gateway Timeout 550 — Permission Denied
505 — HTTP Ver Not Supported 551 — Option Not Supported
506 — Variant Also Negotiates 598 — Nework Read Timeout Error
507 — Insufficient Storage 599 — Network Connect Timeout Error



> ### 4. Хорошо, но как я буду обрабатывать тестовые данные? ###

Зависит от того, откуда вы получаете тестовые данные. Эти тестовые фреймворки позволят вам использовать все возможности языка, на котором они основаны-

a. **База данных:** вы можете легко создать соединения БД для чтения данных.

b. **Внешний файл:** вы можете прочитать внешний текст, JSON, CSV или любые другие файлы.

c. **Случайные данные:** вы можете использовать библиотеки, такие как [faker](https://github.com/marak/Faker.js/), для генерации случайных тестовых данных на лету.


```
var faker = require('faker');

var randomName = faker.name.findName(); // Rowan Nikolaus
var randomEmail = faker.internet.email(); // Kassandra.Haley@erich.biz
var randomCard = faker.helpers.createCard(); // random contact card containing many properties
```

d. **Данные из ответа API :** Много раз во время тестирования вам потребуется передать ответ одного API в качестве данных запроса другому. Вы можете сделать это, используя **hooks**.Вы получите функции, такие как **Before, Before each, After, After each**, которые, как следует из названия, выполняется до/после любого или всех тестов. Вызовите API1 перед API2, и передайте его ответ к API2. Просто направо!!! ☺️




> ### 5. Обработка тестовых данных и выполнение вызовов API кажется простым, но как я собираюсь проверить ответы API? ###

Для проверки правильности ответов вам понадобится библиотека под названием **Assertion library**. Многие тестовые среды поставляются в комплекте с библиотеками assertion, что дает вам возможность писать assert-ы на простом английском языке, таком как синтаксис. Они также позволяют вам проверить [JSON-схему](http://json-schema.org/) вашего ответа.

В mocha вы можете использовать любую библиотеку утверждений, например, [**chai**](http://www.chaijs.com/).

```
response.status.should.equal(200)
foo.should.be.a('string');
foo.should.have.lengthOf(3);
tea.should.have.property('flavors')
  .with.lengthOf(3);
```

> ### 6. Потрясающе!! Осталась мелочь, после всего этого тестирования, как-то показать моему боссу, что я сделал и где нашел проблемы? ###

Большинство этих фреймворков предоставят вам **базовый HTML-отчет** о тестовом прогоне, который вы можете скачать и поделиться им. Если вам нужны более красивые отчеты с графиками и диаграммами, вы можете использовать несколько инструментов отчетности с открытым исходным кодом, таких как [allure](https://github.com/allure-framework/) или [mochawesome](https://adamgruber.github.io/mochawesome/).


![](https://cdn-images-1.medium.com/max/800/1*bY7csygX2PtlExGLAz-y7g.png)

> ### *7. Только если бы я мог получить какойто шаблон с этими вещами, чтобы начать автоматизацию API сейчас* ###


*Что я тебе говорил, ты назвал это, ты получил 👇*

Это шаблон, который я создал для [автоматизации API, используя mocha в node.js](https://github.com/shashikumarraja/api_automation_boilerplate-javascript).

Он включает-
- Test-framework(mocha),
- HTTP request library(supertest),
- Assertion library(chai),
- Reporter(allure),
- logger(winston),
- Random data generator(faker),
- Eslint support and 
- [naughty string validation integrated](https://blog.cloudboost.io/your-api-vs-naughty-strings-72020036a29c). Все, что вам нужно сделать, это clone/download его, и вы готовы к работе.

Если вам нужен шаблон на другом языке, я могу подготовить его к завтрашнему дню. 💤

Если вам понравился этот ответ вы можете похлопать в ладоши, это возможно сподвигнет меня написать больше 🙏

